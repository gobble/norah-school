---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "School Links"
  text: ""
  tagline: “Education is the key to unlocking the world, a passport to freedom.” – Oprah Winfrey 
  actions:
    - theme: brand
      text: MCB Portal
      link: https://shorturl.at/ajzFR
    - theme: alt
      text: Google Classroom
      link: https://classroom.google.com/c/NjAyMzQwMzE0NDcz
      
features:
  - icon: 
      src: /images/raz.png  
    title: RAZ Kids
    link: https://www.kidsa-z.com/main/Login
    details: The award-winning website where K-5 students go to read — anytime, anywhere!
  - icon: 
      src: /images/mindspark.png
    title: MindSpark
    link: https://learn.mindspark.in/Student/onboard/login/en
    details: Ei Mindspark is an AI-powered personalised adaptive online Maths learning platform.
  - icon:
      src: /images/google-meet.png
    title: Google Meet
    link: https://meet.google.com/#
    details: Secure video conferencing by Google for Online Classes.
---

